<?php if ( !defined ('BASEPATH')) exit ('No direct script access allowed');

$plugin_info = array(
    'pi_name' => "SEO Title",
    'pi_version' => '1.0',
    'pi_author' => "Eric Slenk",
    'pi_description' => "Returns an entry's SEO Lite title.",
    'pi_usage' => Seo_title::usage()
);

class Seo_title
{
    private $EE; /**< ExpressionEngine instance. */
    public $return_data = ""; /**< Data to be output by the plugin. */


    # #########
    # Built-ins
    # #########
    /** 
     * Constructor
     */
    public function __construct ()
    {
        // Hook EE
        $this->EE = get_instance();

        // Get the title
        try
        {
            $this->return_data = $this->QueryTitle($this->FetchEntryId());
        }
        catch (Exception $e)
        {
            $this->return_data = FALSE;
        }
    }


    # ##############
    # Data Retrieval
    # ##############
    /**
     * Fetches the entry_id parameter.
     *
     * @return int The entry_id of the entry whose title is to be queried.
     */
    private function FetchEntryId ()
    {
        // Get parameter
        $entry_id = $this->EE->TMPL->fetch_param('entry_id');

        // Validate and return
        if (is_numeric($entry_id) && $entry_id > 0) 
        {
            return (int)$entry_id;
        }
        else
        {
            throw new Exception ("Invalid entry_id.");
        }
    }

    /**
     * Gets the SEO title of the given entry.
     *
     * @param int $entry_id The entry_id of the entry whose SEO title is to queried.
     * @return string The SEO title of the given entry.
     */
    private function QueryTitle ($entry_id)
    {
        // Query
        $result = $this->EE->db
            -> select ('title')
            -> from ('exp_seolite_content')
            -> where ("site_id", $this->EE->config->item('site_id'))
                -> where ("entry_id", $entry_id)
            -> get() -> result_array();
        
        // No results?
        if (empty($result))
            throw new Exception("No SEO Lite content found.");

        return $result[0]['title'];
    }


    # #########
    # Utilities
    # #########
    /**
     * Outputs this plugin's usage string.
     */
    public function usage () 
    {
        // Start an output buffer
        ob_start();

        // Populate the output buffer with usage text
?>
The SEO Title plugin is expected to be used inside of an {exp:channel:entries} loop to print an entry's SEO Lite title.

SYNTAX
    {exp:seo_title entry_id="{entry_id}"}
<?php
        // Get the usage text from the buffer
        $buffer = ob_get_contents();

        // Clear the buffer
        ob_end_clean();

        // Return usage text
        return $buffer;
    }
}
